import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import {Card, CardTitle} from 'react-materialize';
import NavbarComponent from './components/navigationComponents/navbar';
import CompanyList from './components/companyComponent/companyList/companyList';
import CompanyCrud from './components/companyComponent/companyCrud/companyCrud';
import {getCompanies} from './api';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ProductList from './components/productComponent/productList';
import ProductRoute from './components/productComponent/productRoute';


class App extends Component {
  state={
    companies:[],
    limit: 2
  }

  render() {
    return (
      <Router>
        <div className="App">
          <NavbarComponent/>
          {/* <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </header> */}
          {/* <CompanyList/> */}

          <Switch>
            <Route path="/products" render={props => <ProductsConfig {...props}/>}/>
            <Route path="/:id" component={CompanyCrud}/>
            <Route exact path="/" component={CompanyList} />
          </Switch>
        </div>
      </Router>
     )
  }
}

export default App;

function ProductsConfig(props){
  let params = new URLSearchParams(props.location.search);
  console.log(params.get("name"));
  return (
    <ProductRoute {...props} name={params.get("name")} id={params.get("id")}/>
  );
}
