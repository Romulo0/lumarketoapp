export async function insertCompany(company){
    try{

        const miInit = { method: 'POST',
                       headers:{
                            'Content-Type': 'application/json'
                       },
                       body: JSON.stringify(company)
                    };
        const response = await fetch('http://localhost:4001/api/company', miInit);
        console.log(company);
        console.log(response);
        return response.ok;
    } catch(ex){
        console.log(ex);
    }
};

export async function getCompany(id){
    try{
        const response = await fetch(`http://localhost:4001/api/company/${id}`);
        return await response.json();
    } catch(ex){
        console.log(ex);
    }
}

export async function getCompanies(params={}){
    try{
        const query = (params==={})?'':`/?limit=${params['limit']}`;
        const response = await fetch(`http://localhost:4001/api/companies${query}`);
        return await response.json();
    } catch(ex){
        console.log(ex);
    }
}

export async function getProduct(id){
    try{
        const response = await fetch(`http://localhost:4001/api/product/${id}`);
        const data = await response.json();
        return data;
    } catch(ex){
        console.log(ex);
    }
}

export async function getProducts(limit,company){
    try{
        ///?limit=${limit}&
        const query = `?company=${company}`;
        const response = await fetch(`http://localhost:4001/api/products/${query}`);
        const data = await response.json();
        console.log(data);
        return data;
    } catch(ex){
        console.log(ex);
    }
}