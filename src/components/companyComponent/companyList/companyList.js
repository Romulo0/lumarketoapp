import React, { Component } from 'react';
import {Card, CardTitle} from 'react-materialize';
import { Link } from "react-router-dom";
import {getCompanies} from '../../../api';
import './companyList.css';

class CompanyList extends Component {
  state={
    companies:[],
    limit: 2
  }

  componentWillMount(){
    getCompanies({limit:this.state.limit}).then((companies)=> {
      this.setState({companies})
    });
  }
  handleOnClickMore = (event) => {
    const name = event.target.name;
    const value = this.state.limit + 1;
    getCompanies({limit:value}).then((companies)=> {
      this.setState({companies, [name]:value})
    });
  }

  render() {
    const companyList = this.state.companies.map((item, index)=>(
      <li key={index} style={{display:'inline-block', margin:20, width:'30%'}}>
        <Card header={<CardTitle reveal image={"https://s03.s3c.es/imag/_v0/770x420/1/4/7/700x420_amazon.jpg"} waves='light'/>}
          title={item.name}
          reveal={<p>Here is some more information about this product that is only revealed once clicked on.</p>}>
          <ul>
            <li>
              <Link to={`/${item._id}`}>
                <a href="#">See</a>
              </Link>
            </li>
            <li>
              <Link to={{
                pathname: `/products`,
                search: `?name=${item.name}&id=${item._id}`}} >
                <a href="#">View products</a>
              </Link>
            </li>
            <li>
              <Link to={`/about`}>
                <a href="#">About</a>
              </Link>
            </li>
          </ul>
        </Card>
      </li>
    )); 
    const companyTable = this.state.companies.map((item, index) => {
      return(
      <tr>
        <td>
          {index}
        </td>
        <td>
          {item.name}
        </td>
        <td>
          {item.rif.type}
        </td>
        <td>
          {item.rif.rifId}
        </td>
        <td>
          {item.description}
        </td>
        <td>
          {item.contact}
        </td>
        <td>
          <a href='#'>See</a>
        </td>
      </tr>
      )
    })
    return (
        <div  className='container'>
            <h2 style={{fontSize:50, color:'rgba(0,0,0,0.6)', fontWeight:100}}>Companies</h2>
            <hr className='hr'></hr>
          <ul>
            {companyList}
          </ul> 
          <a href='#' onClick={this.handleOnClickMore} name='limit' style={{fontSize:20}}>See More</a>
        </div>)
  }
}

export default CompanyList;
