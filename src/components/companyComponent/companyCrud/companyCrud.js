import React, {Component} from 'react';
import {Card, Input, Modal, Button, Badge, Row} from 'react-materialize';
import { insertCompany, getCompany } from '../../../api';
import './companyCrud.css';

export default class CompanyCrud extends Component{
    state = {
        operation:'Read',
        enable: false,
        id:'',
        name:'',
        type:'RIF',
        rifId:'',
        description:'',
        category: '',
        productLine: '',
        warehouse: '',
        region: '',
        direction: '',
        categories:[],
        productLines:[],
        warehouses:[]
    }

    componentWillMount(){
        if(this.props.match.params.id){
            const params = this.props.match.params;
            console.log(params);
            const id = params.id;
            getCompany(id)
            .then(
                (response)=>{
                    this.setState({
                        id,
                        name: response.name,
                        type: response.rif.type,
                        rifId: response.rif.rifId,
                        categories: response.categories,
                        productLines: response.productLine,
                        warehouses: response.warehouse
                    });
                }
            );
        }
    }

    handleInputChangeOperation(event){
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]:value})
    }

    handleInputChange= (event)=>{
        const name = event.target.name;
        const value = event.target.value;
        let enable = true;
        if(name==='operation'){
            if(value === 'Read' || value==='Delete')
                enable = false;
        }
        this.setState({[name]:value, enable})
    }
    handleOnClick = (event) =>{
        const name = event.target.name;
        let value = '';
        if(name==='categories')
            value = this.state.category
        else if(name==='productLines')
            value = this.state.productLine
        else
            value = {name:this.state.warehouse, region:this.state.region, direction:this.state.direction}
        let valueArray = this.state[name];
        valueArray.push(value);
        this.setState({
            [name]:valueArray 
        })
    }
    handleOnClickSend = ()=>{
         insertCompany({
             name:this.state.name,
             rif:{
                 type:this.state.type,
                 rifId:this.state.rifId
             },
             warehouse:this.state.warehouses,
             productLine: this.state.productLines,
             categories: this.state.categories
        });
    }
    render(){
        const listCategories = this.state.categories.map((item, index)=>(
            <tr key={index}>
                <td>{index}</td>
                <td>{item}</td>
            </tr>
        ));
        const listLines = this.state.productLines.map((item, index)=>(
            <tr key={index}>
                <td>{index}</td>
                <td>{item}</td>
            </tr>
        ));
        const listWarehouses = this.state.warehouses.map((item, index)=>(
            <tr key={index}>
                <td>{index}</td>
                <td>{item.name}</td>
                <td>{item.region}</td>
                <td>{item.direction}</td>
            </tr>
        ));
        return (
            
        <div className="container insertCard">
            <Input type='select' className="operation" label="Operation" name="operation"
                        onChange={this.handleInputChange} defaultValue={this.state.operation} value={this.state.operation}>
                        <option value='Insert'>INSERT</option>
                        <option value='Read'>READ</option>
                        <option value='Update'>UPDATE</option>
                        <option value='Delete'>DELETE</option>
            </Input>
            <Card
                title={this.state.operation + "  Company"}
                reveal={<p>Here goes important information about this company</p>}
                className="hoverable"
                actions={[<Button onClick={this.handleOnClickSend} disabled = {!this.state.enable}>Send</Button>]}>
                <div className="form-group">
                <Row>
                    <Input s={12}label= "Name" className="input" name="name" disabled={!this.state.enable}
                        onChange={this.handleInputChange} value={this.state.name}/>
                </Row>
                <Row>
                    <Input s={3} type='select' className="input" label="Id Type" name="type"  disabled={!this.state.enable}
                        onChange={this.handleInputChange} defaultValue='Rif' value={this.state.type}>
                        <option value='Rif'>RIF</option>
                        <option value='Dni'>DNI</option>
                    </Input>
                        
                    <Input s={7} label= "Id" className="input" name='rifId' disabled={!this.state.enable}
                        onChange={this.handleInputChange} value={this.state.rifId} />
                </Row>
                <Row>
                </Row>
                <Row>    
                    <Input s={12}label= "Description" className="input" name='description' disabled={!this.state.enable}
                        onChange={this.handleInputChange} value={this.state.description} type='textarea'/>
                </Row>
                    <label style={labelStyle}>Add Categories for products</label>
                    <div
                        style={modaSltyle}>
                        <Modal
                            header='Categories'
                            trigger={<Button>Add Category</Button>}>
                            <div>
                                <Input label= "Name" className="input" 
                                    name = "category"
                                    onChange = {this.handleInputChange}
                                    value={this.state.category}/>
                                <Button
                                    name = "categories"
                                    onClick ={this.handleOnClick}  disabled = {!this.state.enable}>Add Category</Button>
                                <table className="striped">
                                    <thead>
                                        <tr>
                                            <th>Nro</th>
                                            <th> Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {listCategories}
                                    </tbody>
                              
                                </table>
                            </div>
                        </Modal>
                    </div>
                    <Badge  className="new badge blue" >{this.state.categories.length} Added</Badge>
                    <label style={labelStyle}>Add Product Lines</label>
                    <div
                    style={modaSltyle}>
                    <Modal
                        header='Product Lines'
                        trigger={<Button>Add Product Line</Button>}>
                        <div>
                            <Input label= "Name" className="input"
                                name="productLine"
                                onChange={this.handleInputChange}/>
                            <Button
                                name="productLines"
                                onClick={this.handleOnClick}  disabled = {!this.state.enable}>
                                Add Category
                            </Button>
                            <table className="striped">
                              <thead>
                                <tr>
                                    <th>Nro</th>
                                    <th>Name</th>
                                </tr>
                              </thead>
                                {listLines}
                              <tbody>
                              </tbody>
                            </table>
                        </div>
                    </Modal>
                    </div>
                    <Badge  className="new badge blue" >{this.state.productLines.length} Added</Badge>
                    <label style={labelStyle}>Add Warehouses to Company</label>
                    <div
                    style={modaSltyle}>
                    <Modal
                        header='Warehouses'
                        trigger={<Button>Add Warehouses</Button>}>
                        <div>
                            <Input label= "Name" style={modalInputSltyle}
                                name="warehouse"
                                onChange={this.handleInputChange}/>
                            <Input label= "Region" style={modalInputSltyle}
                                name="region"
                                onChange={this.handleInputChange}/>
                            <Input label= "Direction" className="input"
                                name="direction"
                                onChange={this.handleInputChange}/>
                            <Button
                                name="warehouses"
                                onClick={this.handleOnClick}  disabled = {!this.state.enable}>
                                Add Category
                            </Button>
                            <table className="striped">
                              <thead>
                                <tr>
                                    <th> Nro</th>
                                    <th> Name</th>
                                    <th> Region</th>
                                    <th> Direction</th>
                                </tr>
                              </thead>
                                {listWarehouses}
                              <tbody>
                              </tbody>
                            </table>
                        </div>
                    </Modal>
                    </div>
                    <Badge  className="new badge blue" >{this.state.warehouses.length} Added</Badge>

                </div>
            </Card>
            </div>
        );
    }
}

const labelStyle = {
    display: 'block',
    fontSize: 15,
    marginBottom: 20
}

const modaSltyle = {
    display: 'inline-block',
    marginBottom: 20
}

const modalInputSltyle = {
    display: 'inline-block !important'
}