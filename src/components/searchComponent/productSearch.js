import React, {Component} from 'react';
import {Input, Row, Col, Card, CardTitle, Icon} from 'react-materialize';

class ProductSearchComponent extends Component {
    render(){
        return(
            <div lassName = "container">
            <div className = "container">
            <Row>
              <Input label="Search Product"><Icon>search</Icon></Input> 
            </Row>
          </div>
          <div className = "container">
          <Row>
          <Col s={3}>
            <Card header={<CardTitle reveal image={"img/office.jpg"} waves='light'/>}
              title="Card Title"
              reveal={<p>Here is some more information about this product that is only revealed once clicked on.</p>}
              className="hoverable">
              <p><a href = "facebook.com" target = "blank">This is a link</a></p>
            </Card>
          </Col>
          <Col s={3}>
            <Card header={<CardTitle reveal image={"img/office.jpg"} waves='light'/>}
              title="Card Title"
              reveal={<p>Here is some more information about this product that is only revealed once clicked on.</p>}
              className="hoverable">
              <p><a href = "facebook.com" target = "blank">This is a link</a></p>
            </Card>
          </Col>
          <Col s={3}>
            <Card header={<CardTitle reveal image={"img/office.jpg"} waves='light'/>}
              title="Card Title"
              reveal={<p>Here is some more information about this product that is only revealed once clicked on.</p>}
              className="hoverable">
              <p><a href = "facebook.com" target = "blank">This is a link</a></p>
            </Card>
          </Col>
          </Row>
          </div>
          </div>
        );
    }
}

export default ProductSearchComponent;