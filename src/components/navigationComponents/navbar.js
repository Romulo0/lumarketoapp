
import React, {Component} from 'react';
import {Navbar, NavItem} from 'react-materialize';
import SideNavComponent from './sideNav';
import { Link } from "react-router-dom";

export default class NavbarComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render(){
        return (
            <Navbar brand={<span style={{marginLeft:30}}className = "deep-orange-text text-lighten-1">Lumarketo</span>} className="blue-grey darken-4" right>
              <NavItem onClick={() => console.log('test click')}>
                <Link to="/">Companies</Link>
              </NavItem>
              <NavItem>
                Products
              </NavItem>
              <NavItem>
                <SideNavComponent/>
              </NavItem>
            </Navbar>
        )
    }
}
