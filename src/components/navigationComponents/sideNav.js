import React, {Component} from 'react';
import {SideNav,SideNavItem, Input, Icon} from 'react-materialize';
class SideNavComponent extends Component {
    render() {
        return (
            <SideNav
            trigger={<div><Icon>list</Icon></div>}
            options={{ closeOnClick: false }}
            >
                <SideNavItem userView
                  user={{
                    background: 'img/office.jpg',
                    image: 'img/yuna.jpg',
                    name: 'John Doe',
                    email: 'jdandturk@gmail.com'
                  }}
                />
                {/* <SideNavItem href='#!second'>Second Link</SideNavItem>
                <SideNavItem divider />
                <SideNavItem subheader>Subheader</SideNavItem>
                <SideNavItem waves href='#!third'>Third Link With Waves</SideNavItem> */}
            </SideNav>
        )
    }
}
export default SideNavComponent;
