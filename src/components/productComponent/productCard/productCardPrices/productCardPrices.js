import React, {Component} from 'react';
import {Row, Col, Card, CardTitle, Input, Button, Icon, Modal} from 'react-materialize';

class ProductCardPricesComponent extends Component {

    constructor(props){
        super(props);
        this.state = {
            noIva: 0.00,
            whithIva: 0.00,
            hasDiscount: false,
            discount: 0.00,
            whithDiscount: 0.00,
            quantity: 0.00,
            price: 0.00
        }
    }
    handleInputChange = (event) => {
        const target = event.target;
        const name = target.name;
        const valid = target.validity.valid;
        let value;
        if(valid) 
            value = target.type === 'checkbox' ? target.checked : target.value;
        else
            value = this.state[name]
        this.setState({
            [name]: value
        });
        console.log(event.target.keyCode)
    }

    handleInputFocus = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        const newValue = value == 0 ? '':value;
        this.setState({
            [name]:newValue
        });
    }
    handleInputBlur = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        const newValue = (value === '' || value == 0)  ? 0:value;
        this.setState({
            [name]:newValue
        });
        console.log(newValue);
    }
    render() {
        return(
            <Card
                title="Product Prices"
               // reveal={<p>Here goes information about the price of this product</p>}
                className="hoverable">
                <Row>
                    <Input s={4} name="noIva" label="Whithout IVA" pattern="[0-9]*"
                        onChange = {this.handleInputChange}
                        onFocus = {this.handleInputFocus}
                        onBlur = {this.handleInputBlur}
                        value={this.state.noIva}/>
                    <Input s={4} name="whithIva" label="Whith IVA" pattern="[0-9]*"
                        onChange = {this.handleInputChange}
                        onFocus = {this.handleInputFocus}
                        onBlur = {this.handleInputBlur}
                        value={this.state.whithIva}/>
                </Row>
                <Row>
                    <Input 
                        type="checkbox" 
                        name="hasDiscount" 
                        label="Has Discount" 
                        onChange = {this.handleInputChange}
                        value={this.state.hasDiscount}/>
                </Row>
                <Row>
                    <Input s={3} 
                        name="discount" 
                        disabled ={!this.state.hasDiscount} 
                        label="Discount" 
                        onChange = {this.handleInputChange}
                        onFocus = {this.handleInputFocus}
                        onBlur = {this.handleInputBlur}
                        value={this.state.discount}
                        pattern="[0-9]*"/>
                    <Input s={3} 
                        name="whithDiscount" 
                        disabled ={!this.state.hasDiscount} 
                        label="Whith Discount" 
                        onChange = {this.handleInputChange}
                        onFocus = {this.handleInputFocus}
                        onBlur = {this.handleInputBlur}
                        value = {this.state.whithDiscount}
                        pattern="[0-9]*"/>
                </Row>
                <Row>
                    <Input s={3} 
                        name="quantity" 
                        type="number"
                        label="Quantity" 
                        onChange={this.handleInputChange}
                        onFocus = {this.handleInputFocus}
                        onBlur = {this.handleInputBlur}
                        value={this.state.quantity}
                        pattern="[0-9]*"/>
                    <Input s={3} 
                        name="price" 
                        label="Total Price" 
                        disabled 
                        onChange={this.handleInputChange}
                        onFocus = {this.handleInputFocus}
                        onBlur = {this.handleInputBlur} 
                        value={this.state.price}
                        pattern="[0-9]*"/>
                </Row>
            </Card>
        );
    }
}

export default ProductCardPricesComponent;