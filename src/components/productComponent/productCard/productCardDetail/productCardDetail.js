import React, {Component} from 'react';
import {Card, Input, Row, Col, Modal, Button} from 'react-materialize';
import './productCardDetail.css';

class ProductCardDetailComponent extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: this.props.product.name,
            number: this.props.product.number,
            detail: this.props.product.detail,
            productLines: this.props.product.productLines[0],
            categories: this.props.product.categories[0],
            hasColor: (this.props.product.hasColor)?'checked':'',
            talla: this.props.product.tallaAndColor.Color,
            color: this.props.product.tallaAndColor.Talla
        }
    }

    handleOnChange = (event)=>{
        console.log(this.state.hasColor);
        const name = event.target.name;
        let value;
        if(name==='hasColor')
            value = (event.target.checked)?'checked':'';
        else
            value = event.target.value;
        console.log(value);
        this.setState({[name]:value})
    }
    render() {
        return (
            <Card
                title="Product Details"
               // reveal={<p>Here goes some information about this product</p>}
                className="hoverable"
                >
                <Row>
                    <Input s={2} label= "Number" name="number" onChange={this.handleOnChange} 
                    disabled={this.props.operation==='Read'}
                    value={this.state.number}/>
                    <Input s={5}label= "Name" name="name" onChange={this.handleOnChange} value={this.state.number}
                    disabled={this.props.operation==='Read'}/>
                </Row>
                <Row>
                    <Input s={6} type="textarea" label= "Description" name="detail" onChange={this.handleOnChange} value={this.state.detail}
                    disabled={this.props.operation==='Read'}/>
                </Row>
                <Row>
                    <Col s={2} className="col-modal">
                        <Modal 
                            className="modal"
                            header='Modal Header'
                            trigger={<Button>...</Button>}>
                            {this.props.modalTable(false)}
                        </Modal>
                    </Col>
                    <Input s={4} label= "Product Line" name="productLines" onChange={this.handleOnChange} value={this.state.productLines}
                    disabled={this.props.operation==='Read'}/>
                    <Col s={2} >
                        <Modal 
                            header='Modal Header'
                            trigger={<Button>...</Button>}>
                            {this.props.modalTable(true)}
                        </Modal>
                    </Col>
                    <Input s={4} label= "Category" name="categories" onChange={this.handleOnChange} value={this.state.categories}
                    disabled={this.props.operation==='Read'}/>
                </Row>
                <Row>
                    <Input name='hasColor' type='checkbox' label= 'Has Color' onChange={this.handleOnChange} checked={this.state.hasColor}
                    disabled={this.props.operation==='Read'}/>
                </Row>
                <Row>
                    <Input s={4} type='select' label="Color" defaultValue='2' name="color" onChange={this.handleOnChange} 
                        value={this.state.color}
                        disabled={this.props.operation==='Read'||this.state.hasColor!=='checked'}>
                      <option value='1'>Yellow</option>
                      <option value='2'>Blue</option>
                      <option value='3'>Red</option>
                    </Input>
                    <Input label= "Talla" name="talla" onChange={this.handleOnChange} value={this.state.talla}
                    disabled={(this.props.operation==='Read'||this.state.hasColor!=='checked')}/>
                </Row>
            </Card>
        );
    }
}

export default ProductCardDetailComponent;