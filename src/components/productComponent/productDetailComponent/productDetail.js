import React, {Component} from 'react';

import {Row, Col,Table, Card, Input, Icon} from 'react-materialize';

import ProductCardDetailComponent from '../productCard/productCardDetail/productCardDetail';
import ProductCardPricesComponent from '../productCard/productCardPrices/productCardPrices';

import {getProduct} from '../../../api';

import './productDetailComponent.css';

const ModalTable = (isProductLine) =>{
    return(
        <Table>
            <thead>
              <tr>
                <th data-field="id">Name</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Alvin</td>
              </tr>
              <tr>
                <td>Alan</td>
              </tr>
              <tr>
                <td>Jonathan</td>
              </tr>
            </tbody>
        </Table>
    );
}
class ProductDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            show:false,
            product: {},
            error:false,
            operation:'Read'
        }
    }
    componentWillMount(){
        if(this.props.match.params.id){
            const id = this.props.match.params.id;
            getProduct(id).then((product)=>{
                console.log(product);
                if(product){
                    this.setState({show:true, product})
                    console.log('succes');}
                else
                    this.setState({show:true,error:true})
            }).catch(err =>{
                console.log(err);
                this.setState({show:true,error:true})
            });
        } else {
            console.log("falta propiedad id");
            this.setState({show:true,error:true});
        }
    }
    handleOnChange = (event)=>{
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]:value});
    }
    render(){
        return(
            <div className="container">
                {this.state.show && (!this.state.error?(
                    <Card
                    title="Product"
                    reveal={<p>Here goes information about the price of this product</p>}
                    className="hoverable">
                    <Row className="operation">
                    <Input type='select' s={5} label="Operation" name="operation"
                                onChange={this.handleOnChange} defaultValue={this.state.operation} value={this.state.operation}>
                                <option value='Insert'>INSERT</option>
                                <option value='Read'>READ</option>
                                <option value='Update'>UPDATE</option>
                                <option value='Delete'>DELETE</option>
                    </Input>
                    </Row>
                    <Row>
                        <Col  s={6}>
                            <ProductCardDetailComponent modalTable = {ModalTable} 
                                product = {this.state.product} 
                                operation= {this.state.operation}/>
                        </Col>
                        <Col  s={6}>
                            <ProductCardPricesComponent modalTable = {ModalTable} 
                                product = {this.state.product}
                                operation= {this.state.operation}/>
                        </Col>
                    </Row>
                    </Card>):
                (
                    <p>Product not found for the given id</p>
                ))}
            </div>
        )
    }
}

export default ProductDetail;