import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Card, CardTitle, Icon} from 'react-materialize';
import {getProducts} from '../../api';
import './productList.css';

class ProductList extends Component {
  state={
    products:[],
    limit: 2
  }

  componentWillMount(){
    getProducts(this.state.limit, this.props.id).then((products)=> {
      this.setState({products})
    });
  }
  handleOnClickMore = (event) => {
    const name = event.target.name;
    const value = this.state.limit + 1;
    getProducts(this.state.limit, this.props.id).then((products)=> {
      this.setState({products, [name]:value})
    });
  }

  render() {
    const evaluation = (points) => {
      let stars = [];
      const emptyStars = 5-points;
      for(let i = 1; i<= emptyStars; i++){
        stars.push(
          <li><Icon key={i} className='deep-orange-text text-lighten-4' tiny>grade</Icon></li>)
      }
      for(let i = 1; i<= points; i++){
        stars.push(
          <li><Icon key={i + emptyStars} className = 'deep-orange-text text-lighten-1' tiny>grade</Icon></li>)
      }
      return (
      <ul style={{display:'flex', flexDirection:'row', justifyContent:'flex-end'}}>
        {stars}
      </ul>
    )}
    const companyList = this.state.products.map((item, index)=>(
      <li key={index} style={{display:'inline-block', margin:20, width:'30%'}}>
        <Card header={<CardTitle reveal image={item.images[0].path} waves='light'/>}
          title={item.name}
          reveal={<p>Here is some more information about this product that is only revealed once clicked on.</p>}>
          <ul>
            <li>
              <Link to={`${this.props.match.url}/${item._id}`}>
                <a href="#">See</a>
              </Link>
            </li>
            <li>
              <Link to={`${this.props.match.url}/about`}>
                <a href="#">About</a>
              </Link>
            </li>
          </ul>
          {evaluation(item.evaluations[0].point)}
        </Card>
      </li>
    )); 
    // const companyTable = this.state.products.map((item, index) => {
    //   return(
    //   <tr>
    //     <td>
    //       {index}
    //     </td>
    //     <td>
    //       {item.name}
    //     </td>
    //     <td>
    //       {item.rif.type}
    //     </td>
    //     <td>
    //       {item.rif.rifId}
    //     </td>
    //     <td>
    //       {item.description}
    //     </td>
    //     <td>
    //       {item.contact}
    //     </td>
    //     <td>
    //       <a href='#'>See</a>
    //     </td>
    //   </tr>
    //   )
    // })
    return (
        <div className='container'>
            <h2 style={{fontSize:50, color:'rgba(0,0,0,0.6)', fontWeight:100}}>Products from {this.props.name}</h2>
            <hr className='hr'></hr>
          <ul>
            {companyList}
          </ul> 
          <a href='#' onClick={this.handleOnClickMore} name='limit' style={{fontSize:20}}>See More</a>
        </div>)
  }
}

export default ProductList;