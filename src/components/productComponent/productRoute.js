import React, {Component} from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import ProductList from './productList';
import ProductDetail from './productDetailComponent/productDetail'
export default class ProductRoute extends Component {
    render(){
        return (
            <Router>
                <div>
                    <Route exact path={this.props.match.path} 
                        render={props => <ProductListConfig {...props} name={this.props.name} id={this.props.id}/>}/>
                    <Route path={`${this.props.match.path}/:id`} component={ProductDetail} />
                </div>
            </Router>
        );
    }
}


function ProductListConfig(props){
    console.log(props)
    return(
        <ProductList {...props}/>
    )
  }